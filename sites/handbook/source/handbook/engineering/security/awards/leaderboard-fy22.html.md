---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1980 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1240 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 3 | 1080 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1000 |
| [@manojmj](https://gitlab.com/manojmj) | 5 | 960 |
| [@engwan](https://gitlab.com/engwan) | 6 | 780 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 7 | 740 |
| [@theoretick](https://gitlab.com/theoretick) | 8 | 700 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 9 | 600 |
| [@vitallium](https://gitlab.com/vitallium) | 10 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 11 | 600 |
| [@leipert](https://gitlab.com/leipert) | 12 | 580 |
| [@mksionek](https://gitlab.com/mksionek) | 13 | 580 |
| [@alexpooley](https://gitlab.com/alexpooley) | 14 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 15 | 500 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 16 | 500 |
| [@stanhu](https://gitlab.com/stanhu) | 17 | 480 |
| [@10io](https://gitlab.com/10io) | 18 | 410 |
| [@whaber](https://gitlab.com/whaber) | 19 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 20 | 400 |
| [@wortschi](https://gitlab.com/wortschi) | 21 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 22 | 400 |
| [@markrian](https://gitlab.com/markrian) | 23 | 360 |
| [@mrincon](https://gitlab.com/mrincon) | 24 | 340 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 25 | 330 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 26 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 27 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 28 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 29 | 300 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 30 | 260 |
| [@dblessing](https://gitlab.com/dblessing) | 31 | 220 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 32 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 33 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 34 | 200 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 35 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 36 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 37 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 38 | 200 |
| [@seanarnold](https://gitlab.com/seanarnold) | 39 | 180 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 40 | 140 |
| [@mwoolf](https://gitlab.com/mwoolf) | 41 | 140 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 42 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 43 | 140 |
| [@kerrizor](https://gitlab.com/kerrizor) | 44 | 130 |
| [@twk3](https://gitlab.com/twk3) | 45 | 130 |
| [@nfriend](https://gitlab.com/nfriend) | 46 | 120 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 48 | 110 |
| [@toupeira](https://gitlab.com/toupeira) | 49 | 110 |
| [@tomquirk](https://gitlab.com/tomquirk) | 50 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 51 | 100 |
| [@cablett](https://gitlab.com/cablett) | 52 | 100 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 53 | 90 |
| [@vsizov](https://gitlab.com/vsizov) | 54 | 80 |
| [@splattael](https://gitlab.com/splattael) | 55 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 56 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 57 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 58 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 59 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 60 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 61 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 62 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 63 | 80 |
| [@acroitor](https://gitlab.com/acroitor) | 64 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 65 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 66 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 67 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 68 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 69 | 60 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 70 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 71 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 72 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 73 | 60 |
| [@minac](https://gitlab.com/minac) | 74 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 75 | 50 |
| [@pslaughter](https://gitlab.com/pslaughter) | 76 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 77 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 78 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 79 | 40 |
| [@afontaine](https://gitlab.com/afontaine) | 80 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 81 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 82 | 30 |
| [@cngo](https://gitlab.com/cngo) | 83 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 84 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 85 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 86 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 87 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@nolith](https://gitlab.com/nolith) | 2 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 3 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 4 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 5 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 80 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 7 | 80 |
| [@aqualls](https://gitlab.com/aqualls) | 8 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 9 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 1000 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 1400 |
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 2 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 3 | 500 |
| [@behrmann](https://gitlab.com/behrmann) | 4 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 5 | 300 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 6 | 300 |
| [@tnir](https://gitlab.com/tnir) | 7 | 200 |

## FY22-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1580 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1000 |
| [@sabrams](https://gitlab.com/sabrams) | 3 | 700 |
| [@vitallium](https://gitlab.com/vitallium) | 4 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 5 | 600 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 6 | 500 |
| [@wortschi](https://gitlab.com/wortschi) | 7 | 400 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 400 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 9 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 10 | 400 |
| [@dblessing](https://gitlab.com/dblessing) | 11 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 12 | 120 |
| [@toupeira](https://gitlab.com/toupeira) | 13 | 110 |
| [@acroitor](https://gitlab.com/acroitor) | 14 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 15 | 80 |
| [@stanhu](https://gitlab.com/stanhu) | 16 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 17 | 80 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 18 | 80 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 19 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 20 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 21 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 22 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 60 |
| [@minac](https://gitlab.com/minac) | 24 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 25 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 60 |
| [@tomquirk](https://gitlab.com/tomquirk) | 27 | 60 |
| [@afontaine](https://gitlab.com/afontaine) | 28 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 29 | 40 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 30 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 31 | 30 |
| [@10io](https://gitlab.com/10io) | 32 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 33 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@smcgivern](https://gitlab.com/smcgivern) | 2 | 40 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 1400 |
| [@behrmann](https://gitlab.com/behrmann) | 2 | 500 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 3 | 300 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


